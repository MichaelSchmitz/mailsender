FROM golang:1.19 AS build
RUN apt-get update && apt-get install ca-certificates -y
RUN update-ca-certificates
WORKDIR /go/src

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY mail ./mail
COPY models ./models
COPY swagger ./swagger
COPY main.go .

ENV CGO_ENABLED=0
RUN go build -a -installsuffix cgo -o app .

FROM scratch AS runtime
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /go/src/app ./
EXPOSE 8080/tcp
ENTRYPOINT ["./app"]
